# Bitcoin Price

### A simple wrapper that provides functions for retrieving the current bitcoin price

## _Created by Frederick John_

## Installation


add `bitcoin_price` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:bitcoin_price, "~> 0.1.1"}
  ]
end
```

Documentation can be found at
[https://hexdocs.pm/bitcoin_price](https://hexdocs.pm/bitcoin_price).
