defmodule BitcoinPrice do
  use HTTPoison.Base
  import Poison
  @api_url "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD"

  @moduledoc """
  Documentation for Bitcoin Price functionality.
  """

  @doc """
   Retrieves the current bitcoin price in $USD at the time of the request

  ## Examples

        iex> BitcoinPrice.current
        #=> 7589.87

  """

  def current() do
    case HTTPoison.get(@api_url) do
      {:ok, %{status_code: 200, body: body}} -> format_price(body) 

      {:ok, %{status_code: 404}} -> "Price not found"# 404 Not Found Error
      {:error, %HTTPoison.Error{reason: reason}} -> IO.inspect reason
    end
  end

  defp format_price(body) do
    {:ok, %{"USD" => usd}} = Poison.decode(body)
    usd
  end

end
