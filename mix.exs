defmodule BitcoinPrice.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bitcoin_price,
      version: "0.1.1",
      elixir: "~> 1.5",
      description: "A simple wrapper for providing bitcoin info including the current price",
      package: package(),
      start_permanent: Mix.env == :prod,
      deps: deps(),

      # Docs
   name: "BitcoinPrice",
   source_url: "https://bitbucket.org/EssenceOfChaos/bitcoin-price/overview",
   homepage_url: "",
   docs: [main: "BitcoinPrice", # The main page in the docs
          logo: "assets/images/bitcoin.png",
          extras: ["README.md"]]
    ]
  end

  def package do
    [ name: :bitcoin_price,
      maintainers: ["Frederick John"],
      licenses: ["MIT"],
      links: %{"BitBucket" => "https://bitbucket.org/EssenceOfChaos/bitcoin_price"},
    ]
  end


  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 0.13.0"},
      {:poison, "~> 3.1"},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false}
    ]
  end
end
