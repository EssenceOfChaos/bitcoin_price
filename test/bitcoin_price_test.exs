defmodule BitcoinPriceTest do
  use ExUnit.Case
  doctest BitcoinPrice

  test "returns an integer" do
    assert BitcoinPrice.current() !== nil
  end
end
